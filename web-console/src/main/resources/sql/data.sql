-- In USERS, the password is admin
insert into USERS (id, username, password, email, status, enabled, last_status_updated_time, created_time, last_updated_time, version) values(1,'admin','$2a$10$4.ouPK9477NVQ7ixRuMbyOKcP3S2RG3v6JaH.TK3ATEdYCG2x.8F6', 'cERzazRuZjEYH5JUtpipChhLrHojxn09', 'ACT', 'Y', current_timestamp , current_timestamp, current_timestamp , 0);
insert into LOGIN_TRIAL(username, trial_count, last_failure_time, last_success_time, version) values ('admin', 0, null, null, 0);
insert into AUTHORITIES (username, authority, create_time) values ('admin','ROLE_ADMIN', current_timestamp);
