package com.mlcodehk.webconsole.enumeration;

import java.util.HashSet;
import java.util.Set;

public enum UserStatus {
    ACT, SUS;

    private static Set<UserStatus> validUserSet = new HashSet<>();

    static {
        validUserSet.add(UserStatus.ACT);
    }

    public static boolean isValidUser(UserStatus status) {
        return validUserSet.contains(status);
    }
}
