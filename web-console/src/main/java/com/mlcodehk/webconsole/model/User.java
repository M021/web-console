package com.mlcodehk.webconsole.model;

import com.mlcodehk.webconsole.enumeration.UserStatus;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.validator.constraints.Length;
import org.jasypt.hibernate5.type.EncryptedStringType;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;


@TypeDef(
        name = "encryptedString",
        typeClass = EncryptedStringType.class,
        parameters = {
                @org.hibernate.annotations.Parameter(name = "encryptorRegisteredName", value = "stringEncryptor")
        }
)

//
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
//
@Table(name = "USERS", uniqueConstraints = {
@UniqueConstraint(columnNames = "USERNAME"),
@UniqueConstraint(columnNames = "EMAIL")})
@Entity
public class User {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @NotEmpty(message = "Username is mandatory")
    @Length(max = 20)
    @Column(nullable = false, length = 20)
    private String username;

    @NotEmpty
    @Column(nullable = false, length = 60)
    private String password;

    @Type(type = "encryptedString")
    @Email
    @NotEmpty(message = "Email is mandatory")
    @Length(max = 50)
    @Column(nullable = false, length = 100)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 3)
    private UserStatus status;

    @NotNull
    @Type(type = "yes_no")
    @Column(nullable = false)
    private Boolean enabled;

    @NotNull
    @Column(nullable = false)
    private Date lastStatusUpdatedTime;

    @NotNull
    @Column(nullable = false)
    private Date createdTime;

    @NotNull
    @Column(nullable = false)
    private Date lastUpdatedTime;

    @Version
    private Long version;

    public boolean isValidUser(){
        return UserStatus.isValidUser(this.getStatus());
    }
}
