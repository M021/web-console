package com.mlcodehk.webconsole.model;

import com.mlcodehk.webconsole.model.embeddable.UsersAuthorityId;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

//
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
//
@Table(name = "AUTHORITIES")
@Entity
public class Authority {

    @EmbeddedId
    private UsersAuthorityId usersAuthorities;

    @NotNull
    @Column(nullable = false)
    private Date createTime;

}
