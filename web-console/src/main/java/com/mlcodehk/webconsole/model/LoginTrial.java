package com.mlcodehk.webconsole.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

//
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
//
@Table(name = "LOGIN_TRIAL")
@Entity
public class LoginTrial {

    @Id
    private String username;

    @NotNull
    @Min(0)
    private Integer trialCount;

    private Date lastFailureTime;

    private Date lastSuccessTime;

    @Version
    private Long Version;

}
