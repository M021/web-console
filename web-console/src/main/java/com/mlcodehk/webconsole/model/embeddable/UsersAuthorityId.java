package com.mlcodehk.webconsole.model.embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

//
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
//
@Embeddable
public class UsersAuthorityId implements Serializable {

    @NotNull
    @Column(nullable = false, length = 20)
    private String username;

    @NotNull
    @Column(nullable = false, length = 20)
    private String authority;

}
