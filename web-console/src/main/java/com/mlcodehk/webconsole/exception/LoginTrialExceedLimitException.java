package com.mlcodehk.webconsole.exception;

import org.springframework.security.core.AuthenticationException;

public class LoginTrialExceedLimitException extends AuthenticationException {

    public LoginTrialExceedLimitException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public LoginTrialExceedLimitException(String msg) {
        super(msg);
    }
}
