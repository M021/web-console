package com.mlcodehk.webconsole.exception;

import org.springframework.security.core.AuthenticationException;

public class AccountNotValidException extends AuthenticationException {

    private static final String msg = "Account is not valid. Please contact administrator.";

    public AccountNotValidException() {
        super(msg);
    }

    public AccountNotValidException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public AccountNotValidException(String msg) {
        super(msg);
    }
}
