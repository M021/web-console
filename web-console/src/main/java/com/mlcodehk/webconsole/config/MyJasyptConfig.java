package com.mlcodehk.webconsole.config;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.jasypt.hibernate5.encryptor.HibernatePBEEncryptorRegistry;
import org.jasypt.salt.SaltGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyJasyptConfig {

    private static final String JASYPT_ENCRYPTOR_PASSWORD = "QWERTYUIOP_ASDFGHJKL_zxcvbnm_1234567890_12121212"; //TODO Modify it
    private static final String JASYPT_ENCRYPTION_ALGORITHM = "PBEWithMD5AndTripleDES";
    private static final String SALT = "pDsk4nf1"; //8 bytes long, //TODO Modify it

    @Bean(name = "stringEncryptor")
    public StringEncryptor getStringEncryptor() {
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword(JASYPT_ENCRYPTOR_PASSWORD);
        config.setAlgorithm(JASYPT_ENCRYPTION_ALGORITHM);
        config.setKeyObtentionIterations("1000");
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        config.setSaltGenerator(getSaltGenerator());
        config.setIvGeneratorClassName("org.jasypt.iv.NoIvGenerator");
        config.setStringOutputType("base64");
        encryptor.setConfig(config);

        HibernatePBEEncryptorRegistry registry = HibernatePBEEncryptorRegistry.getInstance();
        registry.registerPBEStringEncryptor("stringEncryptor", encryptor);

        return encryptor;
    }

    private SaltGenerator getSaltGenerator() {
        return new SaltGenerator() {
            @Override
            public byte[] generateSalt(int lengthBytes) {
                return SALT.getBytes();
            }

            @Override
            public boolean includePlainSaltInEncryptionResults() {
                return true;
            }
        };
    }
}
