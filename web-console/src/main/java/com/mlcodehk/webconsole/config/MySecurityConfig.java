package com.mlcodehk.webconsole.config;

import com.mlcodehk.webconsole.filter.LoginTrialFilter;
import com.mlcodehk.webconsole.handler.CustomAuthenticationFailureHandler;
import com.mlcodehk.webconsole.handler.CustomAuthenticationSuccessHandler;
import com.mlcodehk.webconsole.service.LoginTrialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.sql.DataSource;

@EnableWebSecurity(debug = true)
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @Autowired
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Autowired
    private LoginTrialService loginTrialService;

    @Autowired
    private String homepageUrl;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception { //You may modify the settings for your purpose
        //TODO configure it
        http.authorizeRequests()
                .antMatchers(homepageUrl).permitAll()
                .antMatchers("/resource/**").permitAll()
                .antMatchers("/console/**").hasAuthority("ROLE_ADMIN")
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(new LoginTrialFilter(homepageUrl,
                        loginTrialService, customAuthenticationFailureHandler), UsernamePasswordAuthenticationFilter.class)
                .formLogin()
                .successHandler(customAuthenticationSuccessHandler)
                .failureHandler(customAuthenticationFailureHandler)
                .loginPage(homepageUrl)

                .and()
                .rememberMe()
                .key("remember-me-key")
                .rememberMeParameter("remember-me")
                .rememberMeCookieName("rememberlogin")
                .tokenValiditySeconds(100)

                .and()
                .httpBasic()

                .and()
                .logout()
                .invalidateHttpSession(true)

                .and()
                .exceptionHandling().accessDeniedPage("/access-denied");
    }

}
