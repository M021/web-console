package com.mlcodehk.webconsole.util;

import lombok.Builder;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    @Data
    @Builder
    static class TimeDifference {
        private int day;

        private int hour;

        private int minute;

        private int second;

        public int getAbsoluteDay() {
            return Math.abs(day);
        }

        public int getAbsoluteHour() {
            return Math.abs(hour);
        }

        public int getAbsoluteMinute() {
            return Math.abs(minute);
        }

        public int getAbsoluteSecond() {
            return Math.abs(second);
        }
    }

    public static Date addSecond(Date date, int secondToAdd) {
        return doAdd(date, Calendar.SECOND, secondToAdd);
    }

    public static Date addMinute(Date date, int minuteToAdd) {
        return doAdd(date, Calendar.MINUTE, minuteToAdd);
    }

    public static Date addHour(Date date, int hourToAdd) {
        return doAdd(date, Calendar.HOUR, hourToAdd);
    }

    public static Date subtractSecond(Date date, int secondToSubtract) {
        return doAdd(date, Calendar.SECOND, secondToSubtract);
    }

    public static Date subtractMinute(Date date, int minuteToSubtract) {
        return doSubtract(date, Calendar.MINUTE, minuteToSubtract);
    }

    public static Date subtractHour(Date date, int hourToSubtract) {
        return doSubtract(date, Calendar.HOUR, hourToSubtract);
    }

    private static Date doAdd(Date date, int fieldNumber, int numberToAdd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(fieldNumber, numberToAdd);
        return cal.getTime();
    }

    private static Date doSubtract(Date date, int fieldNumber, int numberToSubtract) {
        return doAdd(date, fieldNumber, -numberToSubtract);
    }

    public static TimeDifference calculateTimeDifference(Date startDate, Date endDate) {

        long diff = startDate.getTime() - endDate.getTime();

        return TimeDifference.builder()
                .day((int) (diff / (24 * 60 * 60 * 1000)))
                .hour((int) (diff / (60 * 60 * 1000) % 24))
                .minute((int) (diff / (60 * 1000) % 60))
                .second((int) (diff / (1000) % 60))
                .build();
    }

    public static Period calculatePeriod(Date startDate, Date endDate) {
        LocalDate startLocalDate = convertDateToLocalDate(startDate);
        LocalDate endLocalDate = convertDateToLocalDate(endDate);
        return Period.between(startLocalDate, endLocalDate);
    }

    public static LocalDate convertDateToLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static Date parseToDate(String date, String dateFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.parse(date);
    }
}
