package com.mlcodehk.webconsole.filter;

import com.mlcodehk.webconsole.common.Constants;
import com.mlcodehk.webconsole.exception.AccountNotValidException;
import com.mlcodehk.webconsole.exception.LoginTrialExceedLimitException;
import com.mlcodehk.webconsole.service.LoginTrialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.thymeleaf.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class LoginTrialFilter implements Filter {

    private LoginTrialService loginTrialService;

    private AuthenticationFailureHandler authenticationFailureHandler;

    private String loginUrl;

    public LoginTrialFilter(String loginUrl, LoginTrialService loginTrialService, AuthenticationFailureHandler authenticationFailureHandler) {
        this.loginUrl = loginUrl;
        this.loginTrialService = loginTrialService;
        this.authenticationFailureHandler = authenticationFailureHandler;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String username = httpServletRequest.getParameter(Constants.usernameParameter);
        String url = httpServletRequest.getRequestURI();
        boolean isCheckRequired = url.equals(loginUrl) && !StringUtils.isEmpty(username);

        if (isCheckRequired) {
            try {
                loginTrialService.checkLoginTrial(username);
            } catch (LoginTrialExceedLimitException | AccountNotValidException | UsernameNotFoundException e) {
                authenticationFailureHandler.onAuthenticationFailure(httpServletRequest, (HttpServletResponse) servletResponse, e);
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }


}
