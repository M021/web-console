package com.mlcodehk.webconsole.repository;

import com.mlcodehk.webconsole.model.LoginTrial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginTrialRepository extends JpaRepository<LoginTrial, String> {
}
