package com.mlcodehk.webconsole.repository;

import com.mlcodehk.webconsole.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select u from User u where u.username = ?1 and u.enabled = 'Y'")
    User findByUsername(String username);

    @Query(value = "select u from User u where u.id = ?1 and u.enabled = 'Y'")
    Optional<User> findById(Long id);

    @Transactional
    @Modifying
    @Query(value = "update VERSIONED User u set u.password = ?2 where u.username = ?1")
    int updatePasswordByUsername(String username, String password);
}
