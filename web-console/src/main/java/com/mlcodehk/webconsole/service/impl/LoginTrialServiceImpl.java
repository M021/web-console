package com.mlcodehk.webconsole.service.impl;

import com.mlcodehk.webconsole.enumeration.UserStatus;
import com.mlcodehk.webconsole.exception.AccountNotValidException;
import com.mlcodehk.webconsole.exception.LoginTrialExceedLimitException;
import com.mlcodehk.webconsole.model.LoginTrial;
import com.mlcodehk.webconsole.model.User;
import com.mlcodehk.webconsole.repository.LoginTrialRepository;
import com.mlcodehk.webconsole.repository.UserRepository;
import com.mlcodehk.webconsole.service.LoginTrialService;
import com.mlcodehk.webconsole.util.DateUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.thymeleaf.util.StringUtils;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@Slf4j
@Getter
@Setter
@Transactional
@Service
public class LoginTrialServiceImpl implements LoginTrialService {

    @Autowired
    private LoginTrialRepository loginTrialRepository;

    @Autowired
    private UserRepository userRepository;

    @Value("${app.setting.web.loginTrial.threshold}")
    private Integer loginTrialThreshold;

    @Value("${app.setting.web.loginTrial.frozenInSecond}")
    private Integer frozenInSecond;

    @Value("${app.setting.web.loginTrial.accountLockThreshold}")
    private Integer accountLockThreshold;

    @Override
    public void checkLoginTrial(String username) throws LoginTrialExceedLimitException, AccountNotValidException, UsernameNotFoundException {

        log.info("Login Trial check - username: " + username);

        Assert.hasText(username, "username cannot be null or empty");

        User user = userRepository.findByUsername(username);

        if (user == null) {
            log.info("User is not found. username :" + username);
            throw new UsernameNotFoundException("Bad credentials"); //return bad credentials message to user.
        }

        if (!user.isValidUser()) {
            throw new AccountNotValidException();
        }

        Optional<LoginTrial> loginTrialOptional = loginTrialRepository.findById(username);

        if (loginTrialOptional.isEmpty()) {
            log.error("Login Trial is not found. username: " + username);
            throw new RuntimeException();
        } else {
            LoginTrial loginTrial = loginTrialOptional.get();

            //check if the trial count is > threshold
            boolean isHitThreshold = (loginTrial.getTrialCount() != 0) && (loginTrial.getTrialCount() % loginTrialThreshold == 0);

            if (isHitThreshold) {
                Date now = new Date();
                Date frozenEndTime = DateUtil.addSecond(loginTrial.getLastFailureTime(), frozenInSecond);
                boolean isFrozen = now.getTime() <= frozenEndTime.getTime();

                if (isFrozen) {
                    throw new LoginTrialExceedLimitException("Attempted many times. Please try later or contact administrator.");
                }
            }
        }
    }

    @Override
    public void recordSuccessLogin(String username) {
        log.trace("Record successful login  - username : " + username);
        Optional<LoginTrial> loginTrialOptional = loginTrialRepository.findById(username);
        Date now = new Date();
        LoginTrial loginTrial;

        if (loginTrialOptional.isEmpty()) {
            log.error("Login Trial is not found. username: " + username);
            throw new RuntimeException();
        } else {
            loginTrial = loginTrialOptional.get();
            loginTrial.setTrialCount(0); //reset to zero
            loginTrial.setLastSuccessTime(now);
        }

        loginTrialRepository.save(loginTrial);
    }

    @Override
    public void recordFailureLogin(String username) {
        log.trace("Record failed login. username : " + username);

        if (StringUtils.isEmpty(username)) {
            return;
        }

        User user = userRepository.findByUsername(username);

        if (user == null) {
            log.error("User is not found. username: " + username);
            throw new RuntimeException();
        }

        Optional<LoginTrial> loginTrialOptional = loginTrialRepository.findById(username);
        Date now = new Date();
        LoginTrial loginTrial;

        if (loginTrialOptional.isEmpty()) {
            log.error("Login Trial is not found. username: " + username);
            throw new RuntimeException();
        } else {
            loginTrial = loginTrialOptional.get();
            int trialCount = loginTrial.getTrialCount() + 1;
            loginTrial.setTrialCount(trialCount);
            loginTrial.setLastFailureTime(now);

            if (trialCount >= accountLockThreshold) {
                user.setStatus(UserStatus.SUS); //lock the account
                user.setLastStatusUpdatedTime(now);
            }
        }
        userRepository.save(user);
        loginTrialRepository.save(loginTrial);
    }
}
