package com.mlcodehk.webconsole.service;

import com.mlcodehk.webconsole.exception.AccountNotValidException;
import com.mlcodehk.webconsole.exception.LoginTrialExceedLimitException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface LoginTrialService {

    void checkLoginTrial(String username) throws LoginTrialExceedLimitException, AccountNotValidException, UsernameNotFoundException;

    void recordSuccessLogin(String username);

    void recordFailureLogin(String username);
}
