package com.mlcodehk.webconsole.handler;

import com.mlcodehk.webconsole.common.Constants;
import com.mlcodehk.webconsole.service.LoginTrialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class CustomAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private LoginTrialService loginTrialService;

    @Autowired
    public CustomAuthenticationSuccessHandler(String defaultSuccessTargetUrl) {
        super();
        super.setDefaultTargetUrl(defaultSuccessTargetUrl);
        super.setAlwaysUseDefaultTargetUrl(true);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws ServletException, IOException {

        String username = request.getParameter(Constants.usernameParameter);

        log.info("Login successful - username: " + username);

        loginTrialService.recordSuccessLogin(username);

        super.onAuthenticationSuccess(request, response, authentication);
    }


}
