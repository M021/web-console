package com.mlcodehk.webconsole.handler;

import com.mlcodehk.webconsole.common.Constants;
import com.mlcodehk.webconsole.service.LoginTrialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Autowired
    private LoginTrialService loginTrialService;

    @Autowired
    public CustomAuthenticationFailureHandler(String defaultFailureTargetUrl) {
        super(defaultFailureTargetUrl);
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        String username = request.getParameter(Constants.usernameParameter);

        log.info("Login failed - username: " + username);

        if (exception instanceof BadCredentialsException) {
            loginTrialService.recordFailureLogin(username);
        }

        super.onAuthenticationFailure(request, response, exception);
    }
}
