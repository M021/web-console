package com.mlcodehk.webconsole.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Slf4j
@Controller
@RequestMapping("/console")
public class ConsoleController {

    @GetMapping("/home")
    public String homepage(Model model, Principal principal) {
        String username = principal.getName();
        model.addAttribute("username", username);
        return "home";
    }
}
