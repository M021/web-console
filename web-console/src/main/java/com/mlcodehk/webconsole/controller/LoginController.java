package com.mlcodehk.webconsole.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Slf4j
@Controller
public class LoginController {

    @GetMapping("/login")
    public String login(Principal principal) {
        if (principal == null) {
            return "login";
        }
        return "redirect:/console/home";

    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "access-denied";

    }
}
