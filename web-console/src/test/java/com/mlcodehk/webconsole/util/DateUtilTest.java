package com.mlcodehk.webconsole.util;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.time.Period;
import java.util.Date;

public class DateUtilTest {

    private final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    @Test
    public void testDayDifference() throws ParseException {
        Date date1 = DateUtil.parseToDate("2021-01-01 10:00:00", DATETIME_PATTERN);
        Date date2 = DateUtil.parseToDate("2021-01-02 10:00:00", DATETIME_PATTERN);

        DateUtil.TimeDifference diff = DateUtil.calculateTimeDifference(date1, date2);

        Assertions.assertEquals(1, diff.getAbsoluteDay());
        Assertions.assertEquals(0, diff.getAbsoluteHour());
        Assertions.assertEquals(0, diff.getAbsoluteMinute());
        Assertions.assertEquals(0, diff.getAbsoluteSecond());
    }

    @Test
    public void testHourDifference() throws ParseException {
        Date date1 = DateUtil.parseToDate("2021-01-01 10:00:00", DATETIME_PATTERN);
        Date date2 = DateUtil.parseToDate("2021-01-02 11:00:00", DATETIME_PATTERN);

        DateUtil.TimeDifference diff = DateUtil.calculateTimeDifference(date1, date2);

        Assertions.assertEquals(1, diff.getAbsoluteDay());
        Assertions.assertEquals(1, diff.getAbsoluteHour());
        Assertions.assertEquals(0, diff.getAbsoluteMinute());
        Assertions.assertEquals(0, diff.getAbsoluteSecond());
    }

    @Test
    public void testMinuteDifference() throws ParseException {
        Date date1 = DateUtil.parseToDate("2021-01-01 10:00:00", DATETIME_PATTERN);
        Date date2 = DateUtil.parseToDate("2021-01-02 11:50:00", DATETIME_PATTERN);

        DateUtil.TimeDifference diff = DateUtil.calculateTimeDifference(date1, date2);

        Assertions.assertEquals(1, diff.getAbsoluteDay());
        Assertions.assertEquals(1, diff.getAbsoluteHour());
        Assertions.assertEquals(50, diff.getAbsoluteMinute());
        Assertions.assertEquals(0, diff.getAbsoluteSecond());
    }

    @Test
    public void testSecondDifference() throws ParseException {
        Date date1 = DateUtil.parseToDate("2021-01-01 10:00:00", DATETIME_PATTERN);
        Date date2 = DateUtil.parseToDate("2021-01-02 11:50:40", DATETIME_PATTERN);

        DateUtil.TimeDifference diff = DateUtil.calculateTimeDifference(date1, date2);

        Assertions.assertEquals(1, diff.getAbsoluteDay());
        Assertions.assertEquals(1, diff.getAbsoluteHour());
        Assertions.assertEquals(50, diff.getAbsoluteMinute());
        Assertions.assertEquals(40, diff.getAbsoluteSecond());
    }

    @Test
    public void testBelow1Year() throws ParseException {
        Date date1 = DateUtil.parseToDate("2021-01-03 10:00:00", DATETIME_PATTERN);
        Date date2 = DateUtil.parseToDate("2021-01-02 11:50:40", DATETIME_PATTERN);

        DateUtil.TimeDifference diff = DateUtil.calculateTimeDifference(date1, date2);

        Assertions.assertEquals(0, diff.getAbsoluteDay());
        Assertions.assertEquals(22, diff.getAbsoluteHour());
        Assertions.assertEquals(9, diff.getAbsoluteMinute());
        Assertions.assertEquals(20, diff.getAbsoluteSecond());
    }

    @Test
    public void testOver1Year() throws ParseException {
        Date date1 = DateUtil.parseToDate("2022-01-03 10:00:00", DATETIME_PATTERN);
        Date date2 = DateUtil.parseToDate("2021-01-02 10:00:00", DATETIME_PATTERN);

        DateUtil.TimeDifference diff = DateUtil.calculateTimeDifference(date1, date2);

        Assertions.assertEquals(366, diff.getAbsoluteDay());
        Assertions.assertEquals(0, diff.getAbsoluteHour());
        Assertions.assertEquals(0, diff.getAbsoluteMinute());
        Assertions.assertEquals(0, diff.getAbsoluteSecond());
    }

    @Test
    public void testCalculatePeriod() throws ParseException {
        Date date1 = DateUtil.parseToDate("2022-01-03 10:00:00", DATETIME_PATTERN);
        Date date2 = DateUtil.parseToDate("2021-01-02 10:00:00", DATETIME_PATTERN);

        Period period = DateUtil.calculatePeriod(date1, date2);

        Assertions.assertEquals(-1, period.getYears());
        Assertions.assertEquals(0, period.getMonths());
        Assertions.assertEquals(-1, period.getDays());
    }

}
