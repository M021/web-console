package com.mlcodehk.webconsole.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.security.Principal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@SpringBootTest(properties = {
        "command.line.runner.enabled=false",
        "application.runner.enabled=false"})
public class LoginControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .build();
    }

    @Test
    @DisplayName("Get login page")
    void get_login_page() throws Exception {
        MvcResult result = mockMvc.perform(
                        get("/login")
                )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    @DisplayName("Accessing login page with principal will be redirected to console homepage")
    void get_login_page_with_principal() throws Exception {

        Principal mockPrincipal = Mockito.mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("test");

        MvcResult result = mockMvc.perform(
                        get("/login")
                                .principal(mockPrincipal)
                )
                .andExpect(redirectedUrl("/console/home"))
                .andExpect(status().is3xxRedirection())
                .andReturn();
    }
}
