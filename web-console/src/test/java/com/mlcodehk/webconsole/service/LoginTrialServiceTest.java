package com.mlcodehk.webconsole.service;

import com.mlcodehk.webconsole.enumeration.UserStatus;
import com.mlcodehk.webconsole.model.LoginTrial;
import com.mlcodehk.webconsole.model.User;
import com.mlcodehk.webconsole.repository.LoginTrialRepository;
import com.mlcodehk.webconsole.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(properties = {
        "command.line.runner.enabled=false",
        "application.runner.enabled=false"})
public class LoginTrialServiceTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private LoginTrialRepository loginTrialRepository;

    @Autowired
    private LoginTrialService loginTrialService;

    private String username = "user 1";

    @BeforeEach
    void setUp() {
        Date now = new Date();
        User user = User.builder()
                .username(username)
                .password("abc")
                .email("abc@email.com")
                .status(UserStatus.ACT)
                .enabled(Boolean.TRUE)
                .createdTime(now)
                .lastStatusUpdatedTime(now)
                .lastUpdatedTime(now)
                .build();
        Mockito.when(userRepository.findByUsername(user.getUsername())).thenReturn(user);

        LoginTrial loginTrial = LoginTrial.builder()
                .username(username)
                .lastFailureTime(null)
                .lastSuccessTime(now)
                .trialCount(0)
                .build();
        Optional<LoginTrial> optional = Optional.of(loginTrial);
        Mockito.when(loginTrialRepository.findById(user.getUsername())).thenReturn(optional);

    }

    @Test
    @DisplayName("The user can pass through the login trial checking.")
    void pass_through_login_trial() {
        loginTrialService.checkLoginTrial(username);
    }

    @Test
    @DisplayName("User not found.")
    void user_not_found() {
        assertThrows(
                UsernameNotFoundException.class,
                () ->
                        loginTrialService.checkLoginTrial("xxx")
        );
    }

}
