package com.mlcodehk.webconsole.jasypt;

import com.mlcodehk.webconsole.config.MyJasyptConfig;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


@Slf4j
public class JasyptTest {

    MyJasyptConfig config = new MyJasyptConfig();

    @Test
    public void encrypt() {
        StringEncryptor stringEncryptor = config.getStringEncryptor();

        String input = "admin123@admin.com";
        String encrypted = stringEncryptor.encrypt(input);
        Assertions.assertNotEquals(input, encrypted);

        String decrypted = stringEncryptor.decrypt(encrypted);
        Assertions.assertEquals(input, decrypted);

        log.info("input: " + input);
        log.info("encrypted: " + encrypted);

    }
}
