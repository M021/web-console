package com.mlcodehk.webconsole.repository;

import com.mlcodehk.webconsole.config.MyJasyptConfig;
import com.mlcodehk.webconsole.enumeration.UserStatus;
import com.mlcodehk.webconsole.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@DataJpaTest
@Import(MyJasyptConfig.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @PersistenceContext
    private EntityManager entityManager;

    User createDummyUser(String name, String password, String email) {
        Date now = new Date();

        User user = User.builder()
                .username(name)
                .password(password)
                .email(email)
                .status(UserStatus.ACT)
                .enabled(Boolean.TRUE)
                .createdTime(now)
                .lastStatusUpdatedTime(now)
                .lastUpdatedTime(now)
                .build();
        return user;
    }

    @Test
    void updatePasswordByUsername() {

        User user = createDummyUser("Tom", "Abcd123", "abc@email.com");
        user = userRepository.save(user);

        entityManager.flush();
        entityManager.clear();

        String newPassword = "ppppppp";
        int result = userRepository.updatePasswordByUsername(user.getUsername(), newPassword);

        entityManager.flush();
        entityManager.clear();

        Optional<User> userFromDb = userRepository.findById(user.getId());

        Assertions.assertEquals(1, result);
        Assertions.assertTrue(userFromDb.isPresent());
        Assertions.assertEquals(newPassword, userFromDb.orElseThrow().getPassword());

    }

    @Test
    void findEnabledUserByUsername() {

        User user1 = createDummyUser("Tom", "Abcd123", "abc@email.com");
        User user2 = createDummyUser("May", "111", "zzz@email.com");
        user2.setEnabled(false);

        user1 = userRepository.save(user1);
        user2 = userRepository.save(user2);

        entityManager.flush();
        entityManager.clear();

        User user1FromDb = userRepository.findByUsername(user1.getUsername());
        User user2FromDb = userRepository.findByUsername(user2.getUsername());

        Assertions.assertNotNull(user1FromDb);
        Assertions.assertNull(user2FromDb);
    }

    @Test
    void findEnabledUserById() {

        User user1 = createDummyUser("Tom", "Abcd123", "abc@email.com");
        User user2 = createDummyUser("May", "111", "zzz@email.com");
        user2.setEnabled(false);

        user1 = userRepository.save(user1);
        user2 = userRepository.save(user2);

        entityManager.flush();
        entityManager.clear();

        Optional<User> user1FromDb = userRepository.findById(user1.getId());
        Optional<User> user2FromDb = userRepository.findById(user2.getId());

        Assertions.assertTrue(user1FromDb.isPresent());
        Assertions.assertTrue(user2FromDb.isEmpty());
    }
}
