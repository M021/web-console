# Web Console

This project is to create a web console with authentication and authorisation using Spring Security. This project is used as a base for building up a customised console or dashboard based on your need. I will use it to build a tools console as an example.

  - [Description](#description)
  - [Resources](#resources)
  - [Source Code](#source-code)
  - [Jasypt command](#jasypt-command)
  - [Reference](#reference)

## Description

In this project, I applied some techniques to enhance the security.

1. Encrypt the database username and password in configuration file by using Jasypt. You may follow below steps to start up tomcat / run spring-boot in order to avoid the decryption password from exposure.
  1. Create a script to export an environment variable.
      touch setEnv.sh
  2. In the script, export an environment variable.
    #!/bin/bash
    export JASYPT_ENCRYPTOR_PASSWORD=<the password for decryption>
  3. Run the setEnv.sh
  4. Run the app in background
  5. delete the setEnv.sh
  6. run below command to unset the environment variable.
     unset JASYPT_ENCRYPTOR_PASSWORD

2. BCryptPasswordEncoder is used to do one-way password encode on the user password.
3. Jasypt is also used to encrypt the email address.


## Resources
Bootstrap [https://getbootstrap.com/](https://getbootstrap.com/)
- used for building web pages.

Jasypt [http://www.jasypt.org/](http://www.jasypt.org/)
- used for encryption

Spring security [https://spring.io/projects/spring-security](https://spring.io/projects/spring-security)
- used for authentication and authorisation

Spring boot [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot)
- used for building an application

Hibernate [https://hibernate.org/](https://hibernate.org/)
- used in data access layer

Spring data [https://spring.io/projects/spring-data](https://spring.io/projects/spring-data)
- used in data access layer


## Source code

In the development, I used h2 in-memory database. You may see that in resources/application.yml. Therefore, you can run the application directly. You may update the application.yml based on your need.

resources/sql contains schema.sql and data.sql. In application.yml, it is configured to run these two sql files automatically during startup.

I implemented login trial feature to prevent from cracking password. You can configure the threshold in application.yml.

I created unit tests on controller layer, service layer and repository layer. You may build up your own based on them.

Please global search "TODO" in the project to see if you need to update those settings.

## Jasypt command
Jasypt-maven-plugin

Go to the project directory and run below command to encrypt or decrypt a string.

-- encrypt a string
mvn jasypt:encrypt-value -Djasypt.encryptor.password=<password> -Djasypt.plugin.value=<text to be encrypted>

-- decrypt an encrypted string
mvn jasypt:decrypt-value -Djasypt.encryptor.password=<password> -Djasypt.plugin.value=<encrypted text>

-- reencrypt those encrypted strings with new password
mvn jasypt:reencrypt -Djasypt.plugin.old.password=<old password> -Djasypt.encryptor.password=<new password> -Djasypt.plugin.path="file:src/main/resources/application.yml"

-- encrypt those strings with DEC() in a file
mvn jasypt:encrypt -Djasypt.encryptor.password=<password> -Djasypt.plugin.path="file:src/main/resources/application.yml"

-- decrypt those strings with DEC() in the directory
mvn jasypt:decrypt -Djasypt.encryptor.password=<password> -D


## Reference
How to use Jasypt to secure the database password
https://jike.in/qa/?qa=459375/
